require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def sub_tring?(str, sub_str)
  str.include?(sub_str)
end

def in_all_strings?(long_strings, substring)
  long_strings.all? {|string| sub_tring?(string, substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = string.split('').sort
  letters.delete(" ")
  letters.reject! { |letter| letters.count(letter) == 1 }
  letters.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  arr = string.split(" ")
  arr = arr.sort_by {|x| x.length}
  [arr[-1], arr[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  result = []
  ('a'..'z').each do |letter|
    if !string.include?(letter)
      result << letter
    end
  end
  result
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = []
  (first_yr..last_yr).each do |year|
    if not_repeat_year?(year)
      years << year
    end
  end
  years
end

def not_repeat_year?(year)
  numbers = year.to_s.split("")
  uniq_numbers = numbers.uniq

  numbers == uniq_numbers
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  uniq_songs = songs.uniq
  uniq_songs.select do |song|
    no_repeats?(song, songs)
  end
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    if song == song_name
      return false if song == songs[idx + 1]
    end
  end
  return true
end


# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  result = ''
  string.delete!'.,!?;'
  words = string.split(' ')
  c_words = words.select { |word1| word1.downcase.include?("c") }
  c_words.each do |word|
    if result == '' || c_distance(word) < c_distance(result)
      result = word
    end
  end
  result
end

def c_distance(word)
  rev_word = word.reverse
  rev_word.index('c')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  start_idx = nil

  arr.each_with_index do |num, idx|
    next_num = arr[idx + 1]
    if num == next_num
      if start_idx == nil
        start_idx = idx
      end
    elsif start_idx != nil
      result << [start_idx, idx]
      start_idx = nil
    end
  end
  result
end











#
